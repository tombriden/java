# Copyright 2009 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require ant java

SUMMARY="Analyze, create, and manipulate Java class files"
DESCRIPTION="
The Byte Code Engineering Library is intended to give users a convenient
possibility to analyze, create, and manipulate (binary) Java class files. Classes
are represented by objects which contain all the symbolic information of the given
class: methods, fields and byte code instructions, in particular. Such objects can
be read from an existing file, be transformed by a program (e.g. a class loader
at run-time) and dumped to a file again. An even more interesting application is
the creation of classes from scratch at run-time. BCEL contains a byte code
verifier named JustIce, which usually gives you much better information about
what's wrong with your code than the standard JVM message.
"
HOMEPAGE="http://commons.apache.org/${PN}/"
DOWNLOADS="mirror://apache/commons/${PN}/source/${PNV}-src.tar.gz"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="freecode:apachebcel sourceforge:bcel"

UPSTREAM_DOCUMENTATION="http://commons.apache.org/${PN}/manual.html [[ lang = en ]]"

LICENCES="Apache-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-java/jakarta-regexp[>=1.5]
"

ANT_SRC_COMPILE_PARAMS=( compile )
ANT_SRC_TEST_PARAMS=( test )

src_prepare() {
    edo mkdir -p "${WORK}"/target/lib/
    edo cp /usr/share/jakarta-regexp/jakarta-regexp-1.5.jar "${WORK}"/target/lib/

    edo sed -i -e 's:\(depends="jar\), javadoc":\1":' build.xml
}

src_compile() {
    export ANT_OPTS=-D"file.encoding=iso-8859-1"
    ant_src_compile
}

src_install() {
    ant_src_install

    dodir /usr/share/${PN}
    insinto /usr/share/${PN}
    doins "${WORK}"/target/${PNV}.jar
}

